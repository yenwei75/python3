import requests
# 引入 Beautiful Soup 模組
from bs4 import BeautifulSoup
# 引入 datatime模組
from datetime import datetime
stockIds = []  # 股票代號
calculteDays = [240, 60, 20]  # 資料的比較天數　例：240<60<20的資料

allPassStockIds = []  # 符合比較條件的股票代號陣列


def getStockIds(strMode):  # 取得所有上櫃的股票
    payload = {'strMode': strMode}
    headers = {  # headers設定 假身份騙過對方我們不是爬蟲
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
        'Accept-Encoding': 'gzip, deflate',
        'Accept-Language': 'zh-TW,zh;q=0.9,en-US;q=0.8,en;q=0.7',
        'Cache-Control': 'no-cache',
        'Connection': 'keep-alive',
        # 'Cookie': 'JSESSIONID=98E1E4476CF53A7EC1AE576D9A616C65',
        'Host': 'isin.twse.com.tw',
        'Pragma': 'no-cache',
        'Upgrade-Insecure-Requests': '1',
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.110 Safari/537.36'
    }
    html_doc = requests.get(
        "http://isin.twse.com.tw/isin/C_public.jsp", params=payload, headers=headers)

    # print(html_doc.text)

    # 以 Beautiful Soup 解析 HTML 程式碼
    soup = BeautifulSoup(html_doc.text, 'html.parser')
    soup = soup.select(" tr > td:nth-of-type(1)")
    for id in soup:
        endPosition = str(id.text).find("　")  # 取得空格所在的位置
        # print(str(id.text[:endPosition]))
        if len(id.text[:endPosition]) is 4:  # 如果空格的位置為4的話就是要取的股票代號
            id = id.text[:endPosition]
            stockIds.append(id)  # 丟入陣列
            print("取得上市證券國際證券辨識號碼"+id)
            if str(id).__eq__("9958"):  # 9958世紀鋼之後的股票代號不取
                break


def calculateSMA(stockIds):  # 取得所有股票的開盤價資料，並過濾條件。

    for index, stockId in enumerate(stockIds):
        if(index is 0):
            print(stockId)
            continue  # 跳過「股票」這個資料
        print("------------------("+stockId+")------------------")
        average = 0
        for index, day in enumerate(calculteDays):
            historyAverage = average  # 將上一次計算完的平均值丟進變數暫存，用於等下比較用。
            # a:股票代號   b:???  c:秀出以今天往前算幾天的資料
            payload = {'a': stockId, 'b': 'D', 'c': day}
            headers = {  # headers設定 假身份騙過對方我們不是爬蟲
                'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
                'Accept-Encoding': 'gzip, deflate',
                'Accept-Language': 'zh-TW,zh;q=0.9,en-US;q=0.8,en;q=0.7',
                'Cache-Control': 'no-cache',
                'Connection': 'keep-alive',
                # 'Cookie': '_ga=GA1.3.1149533070.1544877434; _gid=GA1.3.2013908534.1544877434',
                'Host': 'jdata.yuanta.com.tw',
                'Pragma': 'no-cache',
                'Upgrade-Insecure-Requests': '1',
                'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.110 Safari/537.36'
            }

            html_doc = requests.get("http://jdata.yuanta.com.tw/Z/ZC/ZCW/CZKC1.djbcd",
                                    params=payload, headers=headers)
            if html_doc.status_code == requests.codes.ok:
                # print(html_doc.text)

                # 以 Beautiful Soup 解析 HTML 程式碼
                soup = BeautifulSoup(html_doc.text, 'html.parser')

                # 空格分隔所有資料區塊 時間:[0]  開盤價:[1]  最高價:[2] 最低價:[3]  收盤價:[4]
                if not soup:  # 如果沒資料，就中斷
                    print("股票代號【"+stockId+"】沒有資料")
                    break
                soup = soup.string.split(" ")
                if len(soup) < 5:  # 如果長度小於5
                    print("股票代號【"+stockId+"】沒有開盤價資料")
                    break
                soup = soup[4].split(",")  # 取收盤價區塊的所有數字
                sum = 0  # 總合初使值為0
                sum = float(sum)
                for y in soup:
                    sum += float(y)

                average = sum/len(soup)
                print("股票代號【"+str(stockId)+"】近"+str(day)+"天的收盤價為："+str(average))
                if average < historyAverage:  # 比上一個平均值小就break   因為我們要的是符合240<=60<=20的資料
                    print("【X】股票代號("+str(stockId)+")不符合條件")
                    break
                if str(index+1).__eq__(str(len(calculteDays))):  # 如果RUN的次數等於最大值時　
                    allPassStockIds.append(str(stockId))  # 丟進陣列
                    average = 0  # 重置變數歸0 供下一個股票代號計算
                    print("【O】股票代號("+str(stockId)+")符合條件")

    print("【統計完成】符合條件的股票代號有：")

    # 取得當下時間至秒，用來命名檔案名稱
    txtName = "data_"+str(datetime.now().strftime('%Y_%m_%d %H_%M_%S'))+".txt"
    txt = open(txtName, "a")  # 開啟檔案準備寫入資料
    for allPassStockId in allPassStockIds:
        txt.write("\n"+allPassStockId)
        print(str(allPassStockId)+"，")
    txt.close  # 都寫完全部資料時關閉檔案 不然第2個人要開會變唯讀狀態
    print("執行結束")


print("getStockIds(2) start:")  # 程式起始位置
getStockIds(2)  # 將strMode=2的資料丟入array
print("getStockIds(4) start:")
getStockIds(4)  # 將strMode=4的資料丟入array
calculateSMA(stockIds)  # 都丟完了之後 才進入下一個function
