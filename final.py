from bs4 import BeautifulSoup
from datetime import datetime
from selenium import webdriver
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

stockIds = []  # 股票代號
desired_capabilities = DesiredCapabilities.CHROME  # 修改页面加载策略
# 注释这两行会导致最后输出结果的延迟，即等待页面加载完成再输出
desired_capabilities["pageLoadStrategy"] = "normal"
option = webdriver.ChromeOptions()
option.add_argument("headless")  # 啟用無痕模式 註解此行會打開browser模擬user動作。
driver = webdriver.Chrome(executable_path="./chromedriver",
                          chrome_options=option, desired_capabilities=desired_capabilities)


def getStockIds(strMode):  # 取得所有上櫃的股票
    driver.get("http://isin.twse.com.tw/isin/C_public.jsp?strMode="+strMode)
    html_doc = driver.find_element_by_css_selector(
        "tbody").get_attribute('innerHTML')

    soup = BeautifulSoup(html_doc, 'html.parser')
    soup = soup.select(" tr > td:nth-of-type(1)")  # 取出每個tr中的第1個td
    for id in soup:
        endPosition = str(id.text).find("　")  # 取得空格所在的位置
        # print(str(id.text[:endPosition]))
        if len(id.text[:endPosition]) is 4:  # 如果空格的位置為4的話就是要取的股票代號
            id = id.text[:endPosition]
            stockIds.append(id)  # 丟入陣列
            print("取得上市證券國際證券辨識號碼"+id)
            if str(id) == ("9958"):  # 9958世紀鋼之後的股票代號不取
                break


def calculateSMA(stockIds):  # 取得所有股票的開盤價資料，並過濾條件。
    for index, stockId in enumerate(stockIds):
        if(index is 0):
            print(stockId)
            continue  # 跳過「股票」這個資料
        print("------------------("+stockId+")------------------")
        driver.get(
            "http://www.yuanta.com.tw/pages/content/StockInfo.aspx?Node=cfd49282-f31f-4000-a234-bbc90ce4b751&stock="+stockId)
        # 遇到抓取的dom在iframe裡，必須切換到iframe模式，並定位到id=QuoteInfoFrame
        driver.switch_to.frame("QuoteInfoFrame")
        # 一直等到SMA240這個元素存在時才執行下一行程式
        locator = (By.CSS_SELECTOR, "span[style*='color:#FF1868;'] ")
        WebDriverWait(driver, 10, 0.5).until(
            EC.presence_of_element_located(locator))  # 每0.5秒檢查元素是否存在，等待10秒直到元素可见。
        # driver.switch_to.default_content() #要抓取iframe之外的dom元件，必須切回主文檔模式
        # driver.execute_script("$('#fg0').find('span')[0].innerHTML;")#執行Javascript語法

        html_doc = driver.find_element_by_css_selector(  # 取ID fg0的html
            "body").get_attribute('innerHTML')
        # print('【html_doc】:'+html_doc+'\n')
        # driver.set_window_size(1920, 1080)  # 設定視窗大小
        # driver.save_screenshot(stockId+".png")  # 抓圖並存檔
        soup = BeautifulSoup(html_doc, 'html.parser')
        soup = soup.select("#fg0 span[style!='color:#ff0000;']")
        print(soup[0].text)
        print(soup[2].text)
        print(soup[4].text)
        print(soup[6].text)
        print(soup[8].text)


print("getStockIds(2) start:")  # 程式起始位置
getStockIds(str(2))  # 將strMode=2的資料丟入array
print("getStockIds(4) start:")
getStockIds(str(4))  # 將strMode=4的資料丟入array
calculateSMA(stockIds)  # 都丟完了之後 才進入下一個function
driver.quit()  # 關閉瀏覽器