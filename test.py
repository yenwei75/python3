
from bs4 import BeautifulSoup
from datetime import datetime
from selenium import webdriver
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC


desired_capabilities = DesiredCapabilities.CHROME  # 修改页面加载策略
# 注释这两行会导致最后输出结果的延迟，即等待页面加载完成再输出
desired_capabilities["pageLoadStrategy"] = "normal"
option = webdriver.ChromeOptions()
option.add_argument("headless")  # 啟用無痕模式 註解此行會打開browser模擬user動作。
option.add_argument('--ignore-certificate-errors')
driver = webdriver.Chrome(executable_path="./chromedriver",
                          chrome_options=option, desired_capabilities=desired_capabilities)


driver.get("https://www.yuanta.com.tw/eYuanta/Securities/Node/Index?MainId=00412&C1=2018040404541005&ID=2018040404541005&Level=1&rnd=1555743113995")
# 遇到抓取的dom在iframe裡，必須切換到iframe模式，並定位到id=QuoteInfoFrame
driver.switch_to.frame("YuantaContentFrame")

locator = (By.CSS_SELECTOR, "#fg0 > div:nth-child(1) > div > span:nth-child(1)")
WebDriverWait(driver, 10, 0.5).until(
    EC.presence_of_element_located(locator))  # 每0.5秒檢查元素是否存在，等待10秒直到元素可见。


html_doc = driver.find_element_by_css_selector(
    "#fg0").get_attribute('innerHTML')
# print(html_doc)
soup = BeautifulSoup(html_doc, 'html.parser')
soup = soup.select("div > div > span:nth-child(1)")  # 取出每個tr中的第1個td
print(soup[0])
print(soup[1])
print(soup[2])
print(soup[3])
